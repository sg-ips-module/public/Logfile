<?php

require_once __DIR__ . '/../libs/helper_buffer.php';

// Klassendefinition
class Logfile extends IPSModule
{

    use Logfile_HelperBuffer;

    // Überschreibt die interne IPS_Create($id) Funktion
    public function Create()
    {

        // Diese Zeile nicht löschen.
        parent::Create();

        //Attribute registrieren
        $this->RegisterAttributeString('LogPath', '');
        $this->RegisterAttributeString('LogFile', 'logfile.log');

        //Variablen registrieren
        $this->RegisterPropertyBoolean('Enabled', false);
        $this->RegisterPropertyBoolean('Debug', false);
        $this->RegisterPropertyInteger('UpdateIntervall', 5);
        $this->RegisterPropertyBoolean('MobileFont', false);
        $this->RegisterPropertyBoolean('SortonTop', false);
        $this->RegisterPropertyInteger('LoglineNumbers', 15);
        $this->RegisterPropertyInteger('CutString', 0);

        //HTML
        $this->RegisterPropertyInteger('HTMLBG', -1);
        $this->RegisterPropertyInteger('HTMLFontType', 0);
        $this->RegisterPropertyInteger('HTMLFontColor', 14474460);
        $this->RegisterPropertyInteger('HTMLFontSize', 16);
        $this->RegisterPropertyInteger('HTMLPaddingHigh', 0);
        $this->RegisterPropertyInteger('HTMLPadding1', 180);
        $this->RegisterPropertyInteger('HTMLPadding2', 120);

        //Profil erstellen
        if (!IPS_VariableProfileExists('LOG.LEVEL')) {
            IPS_CreateVariableProfile("LOG.LEVEL", 1);
            IPS_SetVariableProfileIcon("LOG.LEVEL", "Edit");
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 0, 'ALL', '', -1);
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 1, 'ERROR', '', 0xFF0000);
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 2, 'WARNING', '', 0xFFFF00);
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 3, 'SUCCESS', '', 0x00FF00);
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 4, 'NOTIFY', '', 0xFFA500);
            IPS_SetVariableProfileAssociation('LOG.LEVEL', 5, 'CUSTOM', '', 0xFFFFFF);
        }

        //Variablen erstellen
        $this->CreateModulVariables();

        //Timer registrieren
        $this->RegisterTimer('LogTimer', 0, 'LOG_ReadLogfile($_IPS[\'TARGET\']);');
    }

    // Überschreibt die intere IPS_ApplyChanges($id) Funktion
    public function ApplyChanges()
    {
        // Diese Zeile nicht löschen
        parent::ApplyChanges();

        //Register Kernel Messages
        $this->RegisterMessage(0, IPS_KERNELSTARTED);

        //IP-Symcon Kernel bereit?
        if (IPS_GetKernelRunlevel() !== KR_READY) {
            $this->SendDebug(__FUNCTION__, $this->Translate('Kernel is not ready! Kernel Runlevel = ') . IPS_GetKernelRunlevel(), 0);
            return false;
        }

        //Set other logfile for debug!
        #$this->WriteAttributeString('LogFile', 'logfiletest1.log');

        //Check Logfile
        $Result = $this->CheckLogFile();
        if ($Result > 102) {
            $this->SetStatus($Result);
            return false;
        }

        //Instanzen OK?
        if ($this->ReadPropertyBoolean('Enabled') === false) {
            $this->CleanUp();
            $this->SetStatus(104);
            return false;
        }

        //Check OK
        $this->SetStatus(102);

        //Timer-Intervall setzen
        $this->SetMyTimerInterval();

        //Start read logfile
        if ($this->GetBufferX('MultiBuffer_Logfile') === '') {
            $this->ReadLogfile();
        } else {
            $this->CreateHTML();
        }

        return true;
    }

    public function GetConfigurationForm()
    {
        $InfoArray = $this->ModulInfo();
        $Name = $InfoArray['Name'];
        $Version = $InfoArray['Version'];
        $Build = $InfoArray['Build'];
        return '{
        "elements":
        [
            {"type": "Label", "label": "' . $Name . ' - v' . $Version . ' (Build: ' . $Build . ') © by SimonS"},
            {"type": "RowLayout", "items": [
                { "type": "CheckBox", "name": "Enabled", "caption": "Activate timer", "width": "350px" },
                {"type": "NumberSpinner", "name": "UpdateIntervall", "caption": "Update Intervall in seconds (Minimum=3)", "minimum": 3, "maximum": 3600}
            ] },
            {"type": "Label", "label": "HTML settings" },
            {"type": "RowLayout", "items": [
            {"type": "CheckBox", "name": "SortonTop", "caption": "Show new log lines on top", "width": "350px" },
            {"type": "CheckBox", "name": "MobileFont", "caption": "Small font on mobile devices" }
            ] },
            {"type": "RowLayout", "items": [
                {"type": "NumberSpinner", "name": "LoglineNumbers", "caption": "Number of log lines to show (0=unlimited)", "minimum": 0, "maximum": 100, "width": "350px" },
                {"type": "NumberSpinner", "name": "CutString", "caption": "Limit text length from logline (0 = deactivated)", "minimum": 0, "maximum": 50 }
            ] },
            {
                "type": "ExpansionPanel",
                "caption": "HTML Advanced",
                "items": [
                    {"type": "RowLayout", "items": [
                        { "type": "Select", "name": "HTMLFontType", "caption": "Font", "width": "350px",
                            "options": [
                                { "caption": "Default", "value": 0 },
                                { "caption": "Consolas", "value": 1 },
                                { "caption": "Courier", "value": 2 },
                                { "caption": "DejaVu Sans Mono", "value": 3 }
                            ]
                        },
                        {"type": "SelectColor", "name": "HTMLFontColor", "caption": "font color", "width": "350px" }
                    ] },
                    {"type": "RowLayout", "items": [
                        {"type": "NumberSpinner", "name": "HTMLFontSize", "caption": "font size", "width": "350px" },
                        {"type": "SelectColor", "name": "HTMLBG", "caption": "background color" }
                        ] },
                    {"type": "RowLayout", "items": [
                        {"type": "NumberSpinner", "name": "HTMLPadding1", "caption": "Horizontal cell spacing (time)", "width": "350px" },
                        {"type": "NumberSpinner", "name": "HTMLPadding2", "caption": "Horizontal cell spacing (sender)", "width": "350px" },
                        {"type": "NumberSpinner", "name": "HTMLPaddingHigh", "caption": "Cell height" }
                        ] }
                ]
            },
            { "type": "CheckBox", "name": "Debug", "caption": "Debug" }
        ],
        "actions":
        [
            {"type": "RowLayout", "items": [
                {"type": "Button", "label": "Read logfile", "onClick": "LOG_ReadLogfile($id);" }
            ] },
            {"type": "RowLayout", "items": [
                { "type": "Button", "label": "Website", "onClick": "echo \'https://ips.air5.net/\';" },
                { "type": "Button", "label": "Documentation", "onClick": "echo \'https://gitlab.com/sg-ips-module/public/Logfile/-/blob/master/README.md\';" }
            ] }
        ],
        "status":
        [
            {"code": 102, "icon": "active", "caption": "Module is active" },
            {"code": 104, "icon": "inactive", "caption": "Module is inactive" },
            {"code": 201, "icon": "error", "caption": "ERROR: Symcon does not recognize a log path!" },
            {"code": 202, "icon": "error", "caption": "ERROR: Log path is not exist!" },
            {"code": 203, "icon": "error", "caption": "ERROR: Log file does not exist!" },
            {"code": 209, "icon": "error", "caption": "ERROR: Output variable was deleted, cannot create HTML variables!" }
        ]
        }';
    }

    public function ReadLogfile()
    {
        //Check module state
        if ($this->GetStatus() !== 102) {
            $this->Translate('Module is inactive');
            return false;
        }

        $LogPath = $this->ReadAttributeString('LogPath');
        $LogFile = $this->ReadAttributeString('LogFile');
        $Log = $LogPath . $LogFile;

        //Check filehash (faster as always reading the file :)
        if ($this->CheckFileHash($Log)) {
            return false;
        }

        //Check file is empty (example: after rotation at midnight)
        $FileSize = filesize($Log);
        $FileSizeKb = round($FileSize / 1000);
        if (!$FileSize) {
            $this->SetMyDebug(__FUNCTION__ . '()', $Log . ' is empty');
            return false;
        }
        #if ($FileSizeKb > 1023) {
        #    $this->SetMyDebug(__FUNCTION__ . '()', 'ERROR: Logfile too big!');
        #    return false;
        #}

        $this->SetMyDebug(__FUNCTION__ . '()', $Log);

        //Read file
        $FileData = file($Log, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        //Create Array
        foreach ($FileData as $Line) {
            //Filter all lines without "|"
            if (strpos($Line, "|")) {
                //Filter all lines without keywords
                if (strpos($Line, "ERROR") || strpos($Line, "WARNING") || strpos($Line, "SUCCESS") || strpos($Line, "NOTIFY") || strpos($Line, "CUSTOM")) {
                    //Set color from profile to array ;-)
                    $c = 0;
                    if (strpos($Line, "ERROR")) {
                        $c = 1;
                    }
                    if (strpos($Line, "WARNING")) {
                        $c = 2;
                    }
                    if (strpos($Line, "SUCCESS")) {
                        $c = 3;
                    }
                    if (strpos($Line, "NOTIFY")) {
                        $c = 4;
                    }
                    if (strpos($Line, "CUSTOM")) {
                        $c = 5;
                    }
                    if ($c === 0) {
                        $Line = $Line . '|';
                    } else {
                        $Line = $Line . '|' . substr('000000' . dechex(IPS_GetVariableProfile('LOG.LEVEL')["Associations"][$c]['Color']), -6);
                    }
                    $Array[] = explode('|', $Line);
                }
            }
        }

        //Save array to buffer
        if (isset($Array) and is_array($Array)) {
            $Result = $this->SetBufferX('MultiBuffer_Logfile', $Array);
            if ($Result) {
                $this->CreateHTML();
            } else {
                $this->SetMyDebug(__FUNCTION__ . '()', 'ERROR: Buffer limit exceeded!');
                $this->SetMyTimerInterval();
                $this->SetStatus(104);
                return false;
            }
        } else {
            $this->SetMyDebug(__FUNCTION__ . '()', 'ERROR: File is not correct or empty!');
            return false;
        }

    }

    private function CheckFileHash(string $Log)
    {
        //Hash test
        $hash = $this->GetBuffer('Hash');
        #echo $hash;
        if (empty($hash)) {
            $hash = hash_file('md5', $Log);
            $this->SetBuffer('Hash', $hash);
            $this->SetMyDebug(__FUNCTION__ . '()', 'Set hash');
            return false;
        }
        if ($hash === hash_file('md5', $Log)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Logfile unchanged');
            return true;
        }
        $this->SetBuffer('Hash', '');
        return false;
    }

    private function CheckLogFile()
    {
        $LogPath = IPS_GetLogDir();
        $LogFile = $this->ReadAttributeString('LogFile');
        if ($LogPath === '') {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Error: IPS_GetLogDir()');
            return 201;
        }
        if (!is_dir($LogPath)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Error: no directory!');
            return 202;
        }
        if (!file_exists($LogPath . $LogFile)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Error: logfile not exists!');
            return 203;
        }
        $this->SetMyDebug(__FUNCTION__ . '()', $LogPath . $LogFile);
        $this->WriteAttributeString('LogPath', $LogPath);
        $this->CreateModulVariables();
        return 102;
    }

    private function CleanUp()
    {
        $objID = @IPS_GetObjectIDByIdent('LogHTML', $this->InstanceID);
        if ($objID) {
            $this->SetValue('LogHTML', '');
        }
        $this->SetBufferX('MultiBuffer_Logfile', '');
        $this->SetBuffer('Hash', '');
        $this->SetMyDebug(__FUNCTION__ . '()', 'Clean up');
        $this->CreateModulVariables();
    }

    private function CreateModulVariables()
    {
        if (!@$this->GetIDForIdent('Logfilter')) {
            $this->RegisterVariableInteger('Logfilter', 'Filter', 'LOG.LEVEL', 0);
            $this->EnableAction('Logfilter');
        }
        if (!@$this->GetIDForIdent('LogHTML')) {
            $this->RegisterVariableString('LogHTML', $this->Translate('Log lines'), '~HTMLBox', 10);
            IPS_SetIcon($this->GetIDForIdent('LogHTML'), 'Database');
        }
    }

    private function CreateHTML()
    {
        $objID = @IPS_GetObjectIDByIdent('LogHTML', $this->InstanceID);
        if (!$objID) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Modul variable for HTML not found!');
            $this->SetStatus(209);
            return;
        }
        $Array = $this->GetBufferX('MultiBuffer_Logfile'); //Array from Multibuffer
        //Check empty array
        if (!is_array($Array)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'No data!');
            return false;
        }
        //Filter
        $Filterkey = GetValueInteger($this->GetIDForIdent('Logfilter'));
        if ($Filterkey > 0) {
            $Keyword = $this->KeywordFilter($Filterkey);
            foreach ($Array as $Zeile) {
                if (strpos($Zeile['2'], $Keyword)) {
                    #print_r($Zeile);
                    $TmpArray[] = $Zeile;
                }
            }
            if (isset($TmpArray)) {
                $Array = $TmpArray;
            } else {
                $this->SetMyDebug(__FUNCTION__ . '()', 'No lines with this filter!');
                $this->SetValue('LogHTML', '');
                return;
            }

        }
        //Logfile limit
        $Dataset = $this->ReadPropertyInteger('LoglineNumbers');
        if ($Dataset > 0) {
            $Array = array_slice($Array, -$Dataset, $Dataset, true);
        }
        //Limit log line length
        $LineLength = $this->ReadPropertyInteger('CutString');
        if ($LineLength > 0) {
            foreach ($Array as &$Zeile) {
                $Zeile['4'] = substr($Zeile['4'], 0, ($LineLength + 1));
            }
        }
        //New lines sort on top?
        $SortonTop = $this->ReadPropertyBoolean('SortonTop');
        if ($SortonTop) {
            $Array = array_reverse($Array);
        }
        #print_r($Array);

        // HTML Ausgabe generieren
        $HTML_CSS_Style = $this->StyleHTML();
        $TitelArray = array('Typ', 'Zeit', 'Sender', 'Nachricht', 'Test');
        $HTML = $HTML_CSS_Style;
        $HTML .= '<tr><th  class="columA1' . $this->InstanceID . '">' . $TitelArray[0];
        $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[1];
        $HTML .= '</th><th class="columA3' . $this->InstanceID . '">' . $TitelArray[2];
        $HTML .= '</th><th class="columA4' . $this->InstanceID . '">' . $TitelArray[3];
        $HTML .= '</th></tr>';

        foreach ($Array as $Zeile) {
            #$this->SetMyDebug(__FUNCTION__ . '()', $Zeile['4']);
            $HTML .= '<td style="background-color:#' . $Zeile['5'] . '; width: 7px; height: 7px; clear: both; opacity: .7; border-radius: 50%; float:left; text-align:middle; margin-left: 21px; margin-top: 11px; margin-right:10px; }' . $this->InstanceID . '"></td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['0'] . '</td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['3'] . '</td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['4'] . '</td></tr>';
        }
        $HTML .= '</table></html>';

        //HTML-Tabelle in Variable schreiben
        $this->SetMyDebug(__FUNCTION__ . '()', 'Write hosts table variable');
        $this->SetValue('LogHTML', $HTML);
    }

    private function StyleHTML()
    {
        /* Code by Bayaro
        HTML CSS Style definieren (Tabelle, Schrift, Farben, ...) */
        $HTMLFontColor = substr('000000' . dechex($this->ReadPropertyInteger('HTMLFontColor')), -6);
        $HTMLBG = substr('000000' . dechex($this->ReadPropertyInteger('HTMLBG')), -6);
        //Transparent?
        if ($this->ReadPropertyInteger('HTMLBG') < 0) {
            $HTMLBG = 'fade(#FFFFFF, 50%)';
        }
        //Schriftart
        $Fontnumber = $this->ReadPropertyInteger('HTMLFontType');
        switch ($Fontnumber) {
            case 0:
                $FontString = 'font-family: inherit;';
                break;
            case 1:
                $FontString = 'font-family: "Consolas";';
                break;
            case 2:
                $FontString = 'font-family: "Courier";';
                break;
            case 3:
                $FontString = 'font-family: "DejaVu Sans Mono";';
                break;
        }
        $FontString0 = $FontString . 'font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#' . $HTMLFontColor . '; text-align:left;';
        $FontString2 = $FontString . 'font-size:' . ($this->ReadPropertyInteger('HTMLFontSize') + 2) . 'px; color:#' . $HTMLFontColor . '; text-align:left;';

        $Css1 = '<html lang="en">';
        $Css1 = $Css1 . '<style type="text/css">';
        $Css1 = $Css1 . '.hp {width:100%; white-space:nowrap; border-collapse:collapse; border: 1px solid rgba(255, 255, 255, 0.1);}';
        $Css1 = $Css1 . '.hp td' . $this->InstanceID . ' {font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#000000; overflow:hidden; word-break:normal; }';
        $Css1 = $Css1 . '.hp th' . $this->InstanceID . ' {font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#FFFFFF; overflow:hidden; word-break:normal; }';
        $Css1 = $Css1 . '.hp .hp-text' . $this->InstanceID . '{' . $FontString0 . ' background-color:#' . $HTMLBG . '; flex: 1; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px; border-collapse:collapse; border: 1px solid rgba(255, 255, 255, 0.1); }';
        $Css1 = $Css1 . '.hp .columA1' . $this->InstanceID . '{' . $FontString2 . ' width: 15px; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '.hp .columA2' . $this->InstanceID . '{' . $FontString2 . ' width:' . $this->ReadPropertyInteger('HTMLPadding1') . 'px; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '.hp .columA3' . $this->InstanceID . '{' . $FontString2 . ' width:' . $this->ReadPropertyInteger('HTMLPadding2') . 'px; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '.hp .columA4' . $this->InstanceID . '{' . $FontString2 . ' padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '</style>';

        $Css2 = '<style media="screen" type="text/css">';
        $Css2 = $Css2 . '    @media only screen and (max-device-width: 480px) {';
        $Css2 = $Css2 . '.hp td' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp th' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .hp-text' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA1' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA2' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA3' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA4' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '}';
        $Css2 = $Css2 . '</style>';

        $Css3 = '<table class="hp">';

        if ($this->ReadPropertyBoolean('MobileFont')) {
            return $Css1 . $Css2 . $Css3;
        }
        return $Css1 . $Css3;
    }

    private function KeywordFilter(int $Key)
    {
        switch ($Key) {
            case 1:
                return 'ERROR';
                break;
            case 2:
                return 'WARNING';
                break;
            case 3:
                return 'SUCCESS';
                break;
            case 4:
                return 'NOTIFY';
                break;
            case 5:
                return 'CUSTOM';
                break;
        }
    }

    public function RequestAction($Ident, $Value)
    {
        #echo $Ident;
        switch ($Ident) {
            case 'Logfilter':
                SetValue($this->GetIDForIdent($Ident), $Value);
                #echo "Logfilter: " . $Value;
                $this->CreateHTML();
                break;
            default:
                throw new Exception("Invalid ident");
        }
    }

    public function MessageSink($TimeStamp, $SenderID, $Message, $Data)
    {
        if ($Message === IPS_KERNELSTARTED) {
            $this->ApplyChanges();
        }
        return true;
    }

    private function SetMyDebug($Message, $DATA)
    {
        if ($this->ReadPropertyBoolean('Debug')) {
            $this->SendDebug($Message, $DATA, 0);
        }
    }

    private function SetMyTimerInterval()
    {
        if ($this->GetStatus() !== 102) {
            $Interval = 0;
            $this->SetTimerInterval('LogTimer', $Interval);
        } else {
            $Interval = $this->ReadPropertyInteger('UpdateIntervall') * 1000;
            $this->SetTimerInterval('LogTimer', $Interval);
        }
        $this->SetMyDebug(__FUNCTION__ . '()', $Interval / 1000 . 's');
    }

    private function ModulInfo()
    {
        return IPS_GetLibrary('{B10431EF-21B0-3667-E8C5-A65A04712757}');
    }

}
