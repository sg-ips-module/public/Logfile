# Logfile

[![Version](https://img.shields.io/badge/Symcon-PHPModul-red.svg)](https://www.symcon.de/)
[![Version](https://img.shields.io/badge/Modul%20Version-1.1%20Build:220421-blue.svg)]()
[![Version](https://img.shields.io/badge/Symcon%20Version->=%205.3-green.svg)]()
[![Version](https://img.shields.io/badge/Spenden-PayPal-yellow.svg?style=flat-square)](https://www.paypal.com/paypalme2/Simon6714?locale.x=de_DE)

---

Ein Symcon Modul zum Anzeigen der Logdatei Einträge im Webfront

---

## Inhaltsverzeichnis

- [Logfile](#logfile)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Funktionsumfang](#funktionsumfang)
  - [Systemanforderungen](#systemanforderungen)
  - [Installation](#installation)
  - [Autoren](#autoren)
  - [Screenshots](#screenshots)
  - [FAQ](#faq)


## Funktionsumfang

- Das Symcon Modul zeigt Einträge der Datei *logfile.log* tabellarisch im WebFront an
- Neue Einträge werden automatisch durch den enthaltenen Timer aktualisiert
- Meldungen können nach Kategorie gefiltert werden (ALL, ERROR, WARNING, SUCCESS, NOTIFY, CUSTOM)
- Aussehen der HTML Tabelle kann angepasst werden
- Optional: Neue Log-Einträge zu Beginn (oben) anzeigen

## Systemanforderungen

- IP-Symcon ab Version 5.3 (keine Legacy-Console Unterstützung!)

## Installation

Das Modul in IP-Symcon einbinden:

1. In der IP-Symcon Console unter "Kern Instanzen" die "Module Control" Instanz öffnen
2. Auf "Hinzufügen" klicken, hier ist die Modul GIT-URL einzutragen:  
   <https://gitlab.com/SG-IPS-Module/Public/Logfile.git>
3. Nun kann im IPS-Objektbaum (an beliebiger Stelle) eine neue Modul-Instanz hinzufügt werden

## Autoren

- [@SimonS](https://ips.air5.net) - Umsetzung
- [@pitti](https://wilkware.de/ip-symcon-module/) - Danke für  [die Idee](https://community.symcon.de/t/log-messages-im-webfront/46776)

## Screenshots
WebFront:  
![Screenshot WebFront](imgs/WebFront.png)

Konfigurationsformular:  
![Screenshot Formular](imgs/Formular.png)


## FAQ

- Wird das Logfile kontinuierlich gelesen?
  - Nein, es wird der Hashwert der Datei *logfile.log* verglichen, nur bei Änderung wird der Dateiinhalt neu gelesen.
